import math
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from pylab import plot, legend, subplot, grid, xlabel, ylabel, show, title
from fireTS.models import NARX, DirectAutoRegressor
from sklearn.ensemble import RandomForestRegressor
from xgboost import XGBRegressor
from sklearn.model_selection import train_test_split
import numpy as np

x = []
y = []
separator = ','


def get_date_elements(date):
    elements = []
    date_time = date.split(' ')
    if len(date_time) >= 2:
        date_list = date_time[0].split('/')
        time_list = date_time[1].split(':')
        date_list.append(time_list[0])
        return date_list
    else:
        print('Invalid format')
    return []


def pre_process_data(file):
    prepared_file = file.replace('.csv', '_prep.csv')
    with open(file, 'r') as file_in:
        with open(prepared_file, 'w') as file_out:
            for line in file_in:
                if len(line) > 0:
                    elements = line.split(',')
                    if len(elements) > 3:
                        date_elements = get_date_elements(elements[1])

                        file_out.write(','.join(date_elements) + separator + elements[2].replace('CJ-', '') + separator
                                       + elements[3])


def create_net(file):
    data = pd.read_csv(file, header=None, names=['Year', 'Month', 'Day', 'Hour', 'Station', 'CO'])
    data = data.astype({'Year': int, 'Month': int, 'Day': int, 'Hour': int, 'Station': int, 'CO': float})
    # data['Time'] = data.apply(lambda row: str(row['Year']) + '-' +
    #                                   (str(int(row['Day']) + 1) if row['Hour'] == 24 else str(row['Month']) + '-' +
    #                                        (str(int(row['Day']) + 1) if row['Hour'] == 24 else row['Day']) + '-'
    #                                        + ('0' if row['Hour'] == 24 else row['Hour']) + ':00:00', axis=1)
    # # # data['Year'] + '-' + data['Month'] + '-' + data['Day'] + '-' + data['Hour'] + ':00:00'
    # data = data.set_index('Time')
    # data.index = pd.to_datetime(data.index)
    #
    # data.plot(subplots=True)

    print(data.info())
    print(data.describe())
    print(data.head(50))
    x = data.iloc[:, 0:5]
    y = data.iloc[:, 5]
    print('----------------')
    print(x)
    print('----------------')
    print(y)
    data.fillna(0, inplace=True)
    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.33, random_state=42)
    # Build a non-linear autoregression model with exogenous inputs
    # using Random Forest regression as the base model
    narx_mdl = NARX(
        RandomForestRegressor(n_estimators=10),
        auto_order=2,
        exog_order=[2, 2, 2, 2, 2],
        exog_delay=[1, 1, 1, 1, 1])
    narx_mdl.fit(X_train, y_train)
    ypred_narx = narx_mdl.predict(X_test, y_test, step=6)
    # ypred_narx = pd.Series(ypred_narx, index=y_test.index)
    # y_test.plot(label='actual')
    # ypred_narx.plot(label='8-step-ahead prediction')
    # plt.legend()

    mse = narx_mdl.score(X_test, ypred_narx, method='mse')
    print(mse)
    #
    # # Build a general autoregression model and make multi-step prediction directly
    # # using XGBRegressor as the base model
    mdl2 = DirectAutoRegressor(
        XGBRegressor(n_estimators=10),
        auto_order=8,
        exog_order=[2, 2, 2, 2, 2],
        exog_delay=[1, 1, 1, 1, 1],
        pred_step=3)
    mdl2.fit(X_train, y_train)
    ypred2 = mdl2.predict(X_test, y_test)

    mse = narx_mdl.score(X_test, ypred2, method='mse')
    print(mse)

    return ypred_narx, ypred2


# def show_result(net):
#     test_positions = [item[0][1] * 1000.0 for item in net.get_test_data()]

#     all_targets1 = [item[0][0] for item in net.test_targets_activations]
#     allactuals = [item[1][0] for item in net.test_targets_activations]

#     #   This is quick and dirty, but it will show the results
#     subplot(3, 1, 1)
#     plot([i[1] for i in population])
#     title("Population")
#     grid(True)

#     subplot(3, 1, 2)
#     plot(test_positions, all_targets1, 'bo', label='targets')
#     plot(test_positions, allactuals, 'ro', label='actuals')
#     grid(True)
#     legend(loc='lower left', numpoints=1)
#     title("Test Target Points vs Actual Points")

#     subplot(3, 1, 3)
#     plot(range(1, len(net.accum_mse) + 1, 1), net.accum_mse)
#     xlabel('epochs')
#     ylabel('mean squared error')
#     grid(True)
#     title("Mean Squared Error by Epoch")

#     show()

if __name__ == '__main__':
    # pre_process_data('data/CO.csv')
    create_net('data/CO_prep.csv')
