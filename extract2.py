import pandas as pd
import xml.etree.ElementTree as et
import glob

# df_all =  pd.DataFrame( columns = ["timestamp", "location", "CO", "SO", "NO", "NO2", "NOX", "Precipitatii", "Presiunea aerului", "Radiatia solara", "Temperatura aer", "Umiditate relativa", "Viteza vantului"])
# df_all.set_index(["timestamp"])           
cols = ["timestamp", "location", "NO", "NO2", "NOx", "Directia vantului", "Precipitatii", "Presiunea aerului", "Radiatia solara", "Temperatura aer", "Umiditate relativa", "Viteza vantului"] 
            
 
def parse_file(file_name):
    xtree = et.parse(file_name)

    xroot = xtree.getroot() 
   

   # ["timestamp", "location", "CO", "SO", "NO", "NO2", "NOX", "Precipitatii", "Presiunea aerului", "Radiatia solara", "Temperatura aer", "Umiditate relativa", "Viteza vantului"]
   

    df_file = None

    locations = xroot.find("locations") if xroot is not None else None

    for location in locations: 

        location_name = location.attrib.get("name")
        if location_name != "CJ-2" and  location_name != "CJ-4":
            continue

        df_loc = None
        measurements = location.find("measurements") if xroot is not None else None
        
        for measurement in measurements: 

            parameter_name = measurement.attrib.get("parameter")       
            
            if parameter_name == "CO":
                continue
            # print(parameter_name)
            measurement_values = measurement.find("measurementValues") if measurement is not None else None
            
            if measurement_values is not None: 
                
                measurement_value = measurement_values.find("measurementValue") if measurement_values is not None else None              
                if measurement_value.attrib.get('name') == 'Valori orare':            
                    data = measurement_value.find("data") if measurement_value is not None else None                    
                    
                    if data is not None:
                            rows = [] # {}    
                            #data_dict = {}
                            indexes = []
                            for value_node in data:                    

                                usable = value_node.attrib.get("usable")                   

                                if usable == "true":

                                    timestamp = value_node.attrib.get("timestamp")                        
                                    indexes.append(timestamp)
                                    value = value_node.attrib.get("value")
                                    rows.append({"timestamp": timestamp, "location": location_name, "value": value, parameter_name: value})
                                    # dic_key = str(timestamp) + "-" + str(location_name)

                                    # print(dic_key)

                                    # if timestamp in data_dict.keys():
                                    #     data_dict[timestamp][parameter_name] = value # {"timestamp": timestamp, "location": location_name, "value": value, parameter_name: value}
                                    # else:
                                    #     data_dict.update( { timestamp : { "location": location_name, "value": value, parameter_name: value} } )
                                    
                            #         df.append( {"timestamp": timestamp, "location": location_name, "value": value, parameter_name: value}, ignore_index=True )                        
                            # df = pd.DataFrame.from_dict(data_dict)
                            # print(df.describe())
                            #df_all.merge(df,left_on='timestamp', right_on='timestamp')
                            df_cols = ["timestamp", "location"] 
                            df_cols.append(parameter_name)
                            df =  pd.DataFrame(rows, columns = df_cols)
                            
                            # print(df.describe())
                            # print(df.info())
                            #print(df.head())

                            if df_loc is None:
                                df_loc = df
                            else:
                                try:
                                    df_loc = df_loc.merge(df, how='outer', left_on= ["timestamp", "location"], right_on= ["timestamp", "location"])    
                                except:
                                    print('file name with error '+file_name)
                                    print(df.head())
                                
                            # if df_all is None:
                            #     df_all =  pd.DataFrame( rows, columns = ["timestamp", "location", "CO", "SO", "NO", "NO2", "NOX", "Precipitatii", "Presiunea aerului", "Radiatia solara", "Temperatura aer", "Umiditate relativa", "Viteza vantului"])
                            # else: 
                            #     df_all.merge(df, left_on="timestamp", right_on="timestamp")
                    else:
                        print('No data node found')
                else:
                    print('Hourly vals node not found')    
            else:
                print('measurementValues node not found')
        if df_file is None:
            df_file = df_loc
        else:
            # print(df_loc.head())
            # print(df_file.head())
            try:
                df_file = df_file.merge(df_loc[cols] , how='outer', left_on= cols, right_on=cols )  
            except:
                print('df_file file name with error '+file_name)
                print(df_loc.head())
            
            #print(df_file.head())
    #df_file.to_csv(file_name.replace('xml','csv'))
    return df_file        
    # if len(rows) > 0:
    #     out_df = pd.DataFrame(rows, columns = df_cols)
    #     #out_df.to_csv(file_name.replace('xml','csv'))
    #     return out_df
    # else:
    #     print('no data')   

# print(len(data_dict.items()))
# out_df2 = pd.DataFrame(data_dict.items(), columns = df_cols)
# print(str(out_df2.describe()))
# dfg = out_df.groupby("timestamp")
# print(dfg[timestamp])
# print(dfg.describe())

# dfs = []
for file_name in glob.glob("data/*.xml"):
    print(file_name)
    df = parse_file(file_name) 
    with open('all_19.csv', 'a') as f:
          df.to_csv(f, header=False)
# df_all.to_csv('all.csv')

# df_1 = parse_file('data/Raport_2019-09-ox.xml') 
# print(df_1.describe())
# print(df_1.head())
# print(df_1.info())
# df_1.to_csv('all-cj-2-4.csv')
# df_all.shape()
#df_all.info()

