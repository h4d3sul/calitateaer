import pandas as pd
import xml.etree.ElementTree as et
import glob

# df_all =  pd.DataFrame( columns = ["timestamp", "location", "CO", "SO", "NO", "NO2", "NOX", "Precipitatii", "Presiunea aerului", "Radiatia solara", "Temperatura aer", "Umiditate relativa", "Viteza vantului"])
# df_all.set_index(["timestamp"])           


def parse_XML(xml_file, df_cols): 
    """Parse the input XML file and store the result in a pandas 
    DataFrame with the given columns. 
    
    The first element of df_cols is supposed to be the identifier 
    variable, which is an attribute of each node element in the 
    XML data; other features will be parsed from the text content 
    of each sub-element. 
    """
    
    xtree = et.parse(xml_file)
    xroot = xtree.getroot()
    rows = []
    
    for node in xroot: 
        res = []
        res.append(node.attrib.get(df_cols[0]))
        for el in df_cols[1:]: 
            if node is not None and node.find(el) is not None:
                res.append(node.find(el).text)
            else: 
                res.append(None)
        rows.append({df_cols[i]: res[i] 
                     for i, _ in enumerate(df_cols)})
    
    out_df = pd.DataFrame(rows, columns=df_cols)
        
    return 

 
def parse_file(file_name):
    xtree = et.parse(file_name)

    xroot = xtree.getroot() 
   

   # ["timestamp", "location", "CO", "SO", "NO", "NO2", "NOX", "Precipitatii", "Presiunea aerului", "Radiatia solara", "Temperatura aer", "Umiditate relativa", "Viteza vantului"]
   

    df_init = None

    locations = xroot.find("locations") if xroot is not None else None

    for location in locations: 

        location_name = location.attrib.get("name")
        print(location_name)
        measurements = location.find("measurements") if xroot is not None else None
        
        for measurement in measurements: 

            parameter_name = measurement.attrib.get("parameter")       
            print(parameter_name)
            measurement_values = measurement.find("measurementValues") if measurement is not None else None
            
            if measurement_values is not None: 
                
                measurement_value = measurement_values.find("measurementValue") if measurement_values is not None else None              
                if measurement_value.attrib.get('name') == 'Valori orare':            
                    data = measurement_value.find("data") if measurement_value is not None else None                    
                    
                    if data is not None:
                            rows = [] # {}    
                            #data_dict = {}
                            indexes = []
                            for value_node in data:                    

                                usable = value_node.attrib.get("usable")                   

                                if usable == "true":

                                    timestamp = value_node.attrib.get("timestamp")                        
                                    indexes.append(timestamp)
                                    value = value_node.attrib.get("value")
                                    rows.append({"timestamp": timestamp, "location": location_name, "value": value, parameter_name: value})
                                    # dic_key = str(timestamp) + "-" + str(location_name)

                                    # print(dic_key)

                                    # if timestamp in data_dict.keys():
                                    #     data_dict[timestamp][parameter_name] = value # {"timestamp": timestamp, "location": location_name, "value": value, parameter_name: value}
                                    # else:
                                    #     data_dict.update( { timestamp : { "location": location_name, "value": value, parameter_name: value} } )
                                    
                            #         df.append( {"timestamp": timestamp, "location": location_name, "value": value, parameter_name: value}, ignore_index=True )                        
                            # df = pd.DataFrame.from_dict(data_dict)
                            # print(df.describe())
                            #df_all.merge(df,left_on='timestamp', right_on='timestamp')
                            df_cols = ["timestamp", "location"] 
                            df_cols.append(parameter_name)
                            df =  pd.DataFrame(rows, columns = df_cols)
                            
                            # print(df.describe())
                            # print(df.info())
                            # print(df.head())

                            if df_init is None:
                                df_init = df
                            else:
                                df_init = df_init.merge(df, how='outer', left_on='timestamp', right_on='timestamp')    
                            # if df_all is None:
                            #     df_all =  pd.DataFrame( rows, columns = ["timestamp", "location", "CO", "SO", "NO", "NO2", "NOX", "Precipitatii", "Presiunea aerului", "Radiatia solara", "Temperatura aer", "Umiditate relativa", "Viteza vantului"])
                            # else: 
                            #     df_all.merge(df, left_on="timestamp", right_on="timestamp")
                    else:
                        print('No data node found')
                else:
                    print('Hourly vals node not found')    
            else:
                print('measurementValues node not found')                  
    return df_init        
    # if len(rows) > 0:
    #     out_df = pd.DataFrame(rows, columns = df_cols)
    #     #out_df.to_csv(file_name.replace('xml','csv'))
    #     return out_df
    # else:
    #     print('no data')   

# print(len(data_dict.items()))
# out_df2 = pd.DataFrame(data_dict.items(), columns = df_cols)
# print(str(out_df2.describe()))
# dfg = out_df.groupby("timestamp")
# print(dfg[timestamp])
# print(dfg.describe())

# dfs = []
# for file_name in glob.glob("data/*.xml"):
#     print(file_name)
#     parse_file(file_name) 

# df_all.to_csv('all.csv')

df_1 = parse_file('data/Raport_2019-09-ox.xml') 
# print(df_1.describe())
print(df_1.head())
# df_all.shape()
#df_all.info()

